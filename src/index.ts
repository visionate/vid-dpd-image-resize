'use strict';

import * as debug_js from 'debug';
import * as Resource from 'deployd/lib/resource.js';
import * as fs from 'fs';
import * as path from 'path';
import * as sharp from 'sharp';

const debug = debug_js( 'vid-dpd-image-resize' );
let publicDir = '/../../public';

class ImageResizer extends Resource {

  public static clientGeneration = true;

  public static label = 'Image Resizer';

  public static events = [ 'get' ];

  public static basicDashboard = {
    settings: [
      {
        name: 'authorization',
        type: 'checkbox',
        description: 'Do you require user to be logged-in to view / upload / delete files ?'
      }
    ]
  };

  public config;

  public name;

  public clientGeneration = true;

  constructor( options ) {
    super( options );

    this.config = {
      authorization: this.config.authorization || false
    };

    Resource.apply( this, arguments );
  }

  public handle( ctx, next ) {

    const req = ctx.req;
    const self = this;
    const domain: { url: any; data?: any } = { url: ctx.url };
    const me = ctx.session.user;

    if ( this.config.authorization && !me ) {
      return ctx.done( {
        statusCode: 403,
        message: 'You\'re not authorized to upload / modify files.'
      } );
    }

    if ( ctx.req.method === 'GET' ) {

      this.get( ctx, function ( err, result ) {
        if ( err ) {
          return ctx.done( err );
        } else if ( (ImageResizer.events as any).get ) {
          domain.data = result;
          domain[ 'this' ] = result;

          (ImageResizer.events as any).get.run( ctx, domain, function ( err ) {
            if ( err ) return ctx.done( err );
            ctx.done( null, result );
          } );
        } else {
          ctx.done( err, result );
        }
      } );

    } else {
      next();
    }
  }

  public get( ctx, next ) {

    const fileName = ctx.query.fileName;
    if ( !fileName || fileName === null || fileName === '' ) {
      ctx.done( { 'error': 'please provide "fileName"' } );
    } else {

      const filePath = path.join( __dirname, publicDir, fileName );
      // If the file doesn't exists ...
      try {
        fs.statSync( filePath ).isFile();
      } catch ( er ) {
        return ctx.done( { 'error': 'file not found' } );
      }

      const outNameArray = fileName.split( '.' );
      outNameArray[ outNameArray.length - 2 ] += '_resized';
      const outName = outNameArray.join( '.' );
      const outPath = path.join( __dirname, publicDir, outName );
      const widthString = ctx.query.width;
      const heightString = ctx.query.height;

      let width, height;

      if ( widthString ) {
        width = parseInt( widthString );
      }

      if ( heightString ) {
        height = parseInt( heightString );
      }

      sharp( filePath )
        .resize( width, height )
        .toFile( outPath )
        .then( info => {
          fs.unlink( filePath, ( errunlink ) => {
            if ( errunlink ) return ctx.done( { 'errorunlink': errunlink.toString() } );
            fs.rename( outPath, filePath, ( err ) => {
              if ( err ) return ctx.done( { 'rename': err.toString() } );
              return ctx.done( null, {
                'result': {
                  'fileName': fileName,
                  'width': info.width,
                  'height': info.height
                }
              } );
            } );
          } );
        } )
        .catch( err => {
          return ctx.done( {
            'error': 'resize failed',
            'message': err.toString()
          } );
        } );
    }
  }
}

module.exports = ImageResizer;
