'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var debug_js = require("debug");
var Resource = require("deployd/lib/resource.js");
var fs = require("fs");
var path = require("path");
var sharp = require("sharp");
var debug = debug_js('vid-dpd-image-resize');
var publicDir = '/../../public';
var ImageResizer = /** @class */ (function (_super) {
    __extends(ImageResizer, _super);
    function ImageResizer(options) {
        var _this = _super.call(this, options) || this;
        _this.clientGeneration = true;
        _this.config = {
            authorization: _this.config.authorization || false
        };
        Resource.apply(_this, arguments);
        return _this;
    }
    ImageResizer.prototype.handle = function (ctx, next) {
        var req = ctx.req;
        var self = this;
        var domain = { url: ctx.url };
        var me = ctx.session.user;
        if (this.config.authorization && !me) {
            return ctx.done({
                statusCode: 403,
                message: 'You\'re not authorized to upload / modify files.'
            });
        }
        if (ctx.req.method === 'GET') {
            this.get(ctx, function (err, result) {
                if (err) {
                    return ctx.done(err);
                }
                else if (ImageResizer.events.get) {
                    domain.data = result;
                    domain['this'] = result;
                    ImageResizer.events.get.run(ctx, domain, function (err) {
                        if (err)
                            return ctx.done(err);
                        ctx.done(null, result);
                    });
                }
                else {
                    ctx.done(err, result);
                }
            });
        }
        else {
            next();
        }
    };
    ImageResizer.prototype.get = function (ctx, next) {
        var fileName = ctx.query.fileName;
        if (!fileName || fileName === null || fileName === '') {
            ctx.done({ 'error': 'please provide "fileName"' });
        }
        else {
            var filePath_1 = path.join(__dirname, publicDir, fileName);
            // If the file doesn't exists ...
            try {
                fs.statSync(filePath_1).isFile();
            }
            catch (er) {
                return ctx.done({ 'error': 'file not found' });
            }
            var outNameArray = fileName.split('.');
            outNameArray[outNameArray.length - 2] += '_resized';
            var outName = outNameArray.join('.');
            var outPath_1 = path.join(__dirname, publicDir, outName);
            var widthString = ctx.query.width;
            var heightString = ctx.query.height;
            var width = void 0, height = void 0;
            if (widthString) {
                width = parseInt(widthString);
            }
            if (heightString) {
                height = parseInt(heightString);
            }
            sharp(filePath_1)
                .resize(width, height)
                .toFile(outPath_1)
                .then(function (info) {
                fs.unlink(filePath_1, function (errunlink) {
                    if (errunlink)
                        return ctx.done({ 'errorunlink': errunlink.toString() });
                    fs.rename(outPath_1, filePath_1, function (err) {
                        if (err)
                            return ctx.done({ 'rename': err.toString() });
                        return ctx.done(null, {
                            'result': {
                                'fileName': fileName,
                                'width': info.width,
                                'height': info.height
                            }
                        });
                    });
                });
            })
                .catch(function (err) {
                return ctx.done({
                    'error': 'resize failed',
                    'message': err.toString()
                });
            });
        }
    };
    ImageResizer.clientGeneration = true;
    ImageResizer.label = 'Image Resizer';
    ImageResizer.events = ['get'];
    ImageResizer.basicDashboard = {
        settings: [
            {
                name: 'authorization',
                type: 'checkbox',
                description: 'Do you require user to be logged-in to view / upload / delete files ?'
            }
        ]
    };
    return ImageResizer;
}(Resource));
module.exports = ImageResizer;
